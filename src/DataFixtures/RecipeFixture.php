<?php

namespace App\DataFixtures;

use App\Entity\Category;
use App\Entity\Ingredient;
use App\Entity\Quantity;
use Faker\Factory;
use App\Entity\Recipe;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use FakerRestaurant\Provider\fr_FR\Restaurant;
use Symfony\Component\String\Slugger\SluggerInterface;

class RecipeFixture extends Fixture implements DependentFixtureInterface
{
    public function __construct(private readonly SluggerInterface $slugger)
    {
    }


    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create('fr_FR');
        $faker->addProvider(new Restaurant($faker));

        $ingredients = array_map(fn (string $name) => (new Ingredient())
            ->setName($name)
            ->setSlug(strtolower($this->slugger->slug($name))), [
            'Farine',
            'Œufs',
            'Lait',
            'Sucre',
            'Beurre',
            'Sel',
            'Poivre',
            'Huile de Tounresol',
            'Oignon',
            'Ail',
            'Tomates',
            'Pommes de terre',
            'Carottes',
            'Poulet',
            'Poisson',
            'Riz',
            'Pâtes',
            'Herbes aromatiques',
            'Fromage',
            'Crème fraîche',
        ]);

        $unit = [
            "g",
            "kg",
            "l",
            "ml",
            "cl",
            "cas",
            "cac",
            "pincée",
            "verre",
        ];

        foreach ($ingredients as $ingredient) {
            $manager->persist($ingredient);
        }
        $categories = ['Plat Principal', 'Entrée', 'Dessert', 'Gouter'];
        foreach ($categories as $cat) {
            $categorie = (new Category)
                ->setName($cat)
                ->setSlug($this->slugger->slug($cat))
                ->setUpdatedAt(\DateTimeImmutable::createFromMutable($faker->dateTime()))
                ->setCreatedAt(\DateTimeImmutable::createFromMutable($faker->dateTime()));
            $manager->persist($categorie);
            $this->addReference($cat, $categorie);
        }
        for ($i = 0; $i < 10; $i++) {
            $title = $faker->foodName();
            $recipe = (new Recipe())
                ->setTitle($title)
                ->setSlug($this->slugger->slug($title))
                ->setUpdatedAt(\DateTimeImmutable::createFromMutable($faker->dateTime()))
                ->setCreatedAt(\DateTimeImmutable::createFromMutable($faker->dateTime()))
                ->setContent($faker->paragraph(10, true))
                ->setCategory($this->getReference($faker->randomElement($categories)))
                ->setDuration($faker->numberBetween(2, 60))
                ->setUser($this->getReference(('USER' . $faker->numberBetween(1, 10))));

            foreach ($faker->randomElement($ingredients, $faker->numberBetween(2, 5)) as $ingredient) {
                $recipe->addQuantity((new Quantity())
                        ->setQuantity($faker->numberBetween(1, 250))
                        ->setUnit($faker->randomElement($unit))
                        ->setIngredient($ingredient)
                );
            }
            $manager->persist($recipe);
        }

        $manager->flush();
    }

    public function getDependencies()
    {
        return [AppFixtures::class];
    }
}
