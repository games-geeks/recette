<?php

namespace App\Controller\Admin;

use App\Entity\Recipe;
use App\Form\RecipeType;
use App\Repository\RecipeRepository;
use App\Security\Voter\ReciperVoter;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Routing\Requirement\Requirement;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\UX\Turbo\TurboBundle;

#[Route("/admin/recettes", name: 'admin.recipe.')]
// #[IsGranted('ROLE_ADMIN')]
class RecipeController extends AbstractController
{

    #[Route('/', name: 'index')]
    #[IsGranted(ReciperVoter::LIST)]
    public function index(Request $request, RecipeRepository $recipeRepository, Security $security): Response
    {

        $page = $request->query->getInt('page', 1);
        $userId = $security->getUser()->getId();
        $canListAll = $security->isGranted(ReciperVoter::LIST_ALL);
        $recipes = $recipeRepository->paginateRecipes($page, $canListAll ? null : $userId);

        return $this->render('admin/recipe/index.html.twig', [
            'recipes' => $recipes
        ]);
    }

    #[Route('/create', name: 'create')]
    #[IsGranted(ReciperVoter::CREATE)]
    public function create(Request $request, EntityManagerInterface $em)
    {
        $recipe =  new Recipe;
        $form = $this->createForm(RecipeType::class, $recipe);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($recipe);
            $em->flush();
            $this->addFlash('success', 'La recette a été créée');
            return $this->redirectToRoute('admin.recipe.index');
        }
        return $this->render(
            'admin/recipe/create.html.twig',
            compact('form')
        );
    }
    // #[Route('/{slug}-{id}', name: 'show', requirements: ['id' => '\d+', 'slug' => '[a-z0-9-]+'])]
    // public function show(Request $request, string $slug, int $id, RecipeRepository $recipeRepository): Response
    // {

    //     $recipe = $recipeRepository->find($id);
    //     if ($recipe->getSlug() != $slug)
    //         return $this->redirectToRoute('recipe.show', ['slug' => $recipe->getSlug(), 'id' => $recipe->getId()]);
    //     return $this->render(
    //         'recipe/show.html.twig',
    //         compact('recipe')
    //     );
    // }

    #[Route('/{id}', name: 'edit', requirements: ['id' => Requirement::DIGITS], methods: ['GET', 'POST'])]
    #[IsGranted(ReciperVoter::EDIT, subject: 'recipe')]
    public function edit(Request $request, Recipe $recipe, RecipeRepository $recipeRepository, EntityManagerInterface $em)
    {
        $form = $this->createForm(RecipeType::class, $recipe);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em->flush();
            $this->addFlash('success', 'La recette a été modifiée');
            return $this->redirectToRoute('admin.recipe.index');
        }
        return $this->render(
            'admin/recipe/edit.html.twig',
            compact('recipe', 'form')
        );
    }



    #[Route('/{id}', name: 'delete', requirements: ['id' => Requirement::DIGITS], methods: ['DELETE'])]
    #[IsGranted(ReciperVoter::EDIT, subject: 'recipe')]
    public function delete(Request $request, Recipe $recipe,  EntityManagerInterface $em)
    {
        $recipeId = $recipe->getId();
        $message = 'La recette a été supprimée';
        $em->remove($recipe);
        $em->flush();
        if ($request->getPreferredFormat() === TurboBundle::STREAM_FORMAT) {
            $request->setRequestFormat(TurboBundle::STREAM_FORMAT);
            return $this->render('admin/recipe/delete.html.twig', ['recipeId' => $recipeId, 'message' => $message]);
        }
        $this->addFlash('success', $message);
        return $this->redirectToRoute('admin.recipe.index');
    }
}
