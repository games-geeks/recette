<?php

namespace App\Controller\API;

use App\Entity\Recipe;
use App\DTO\PaginationDTO;
use Doctrine\ORM\EntityManager;
use App\Repository\RecipeRepository;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Routing\Requirement\Requirement;
use Symfony\Component\HttpKernel\Attribute\MapQueryString;
use Symfony\Component\HttpKernel\Attribute\MapRequestPayload;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class RecipeController extends AbstractController
{
    #[Route('/api/recipes', methods: ['GET'])]
    public function index(
        RecipeRepository $recipeRepository,
        #[MapQueryString()]
        ?PaginationDTO $paginationDTO = null
    ) {
        $recipes = $recipeRepository->paginateRecipes($paginationDTO?->page);
        return $this->json($recipes, 200, [], [
            'groups' => ['recipes.index']
        ]);
    }


    #[Route('/api/recipes/{id}',  requirements: ['id' => Requirement::DIGITS], methods: ['GET', 'POST'])]
    public function show(Recipe $recipe)
    {
        return $this->json($recipe, 200, [], [
            'groups' => ['recipes.index', 'recipes.show']
        ]);
    }

    #[Route('/api/recipes',  methods: ['POST'])]
    public function create(
        EntityManager $em,
        #[MapRequestPayload(
            serializationContext: [
                'groups' => ['recipes.create']
            ]
        )]
        Recipe $recipe
    ) {
        $recipe->setCreatedAt(new \DateTimeImmutable());
        $recipe->setUpdatedAt(new \DateTimeImmutable());
        $em->persist($recipe);
        $em->flush();
        return $this->json($recipe, 200, [], [
            'groups' => ['recipes.index', 'recipes.show']
        ]);
    }
}
