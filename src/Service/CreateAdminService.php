<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\User;
use App\Repository\UserRepository;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class CreateAdminService
{
    public function __construct(
        private readonly UserRepository $userRepository,
        private readonly UserPasswordHasherInterface $userPasswordHasher
    ) {
    }

    public function create(string $email, string $password, string $username): void
    {
        $user = $this->userRepository->findOneBy(['email' => $email]);

        // On crée user si il n'existe pas
        if (!$user) {
            $user = new User();
            $user->setEmail($email);
            $user->setUsername($username);
            $password = $this->userPasswordHasher->hashPassword($user, $password);
            $user->setPassword($password);
        }
        // Dans tous les cas, on lui donne les droits admin
        $user->setRoles(['ROLE_ADMIN']);

        $this->userRepository->save($user, true);
    }
}
